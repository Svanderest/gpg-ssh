# GPG SSH

Install GNU Privacy Guard

Start the agent with ssh support

    eval $( gpg-agent --daemon --enable-ssh-support )

Import key from file

    gpg --import key-file

Get the keygrip for the imported key

    gpg --list-keys --with-keygrip

Write the keygrip to ~/.gnupg/sshcontrol
